from django.shortcuts import render


def index(request):
    return render(request, 'main/index.html')

def exp(request):
    return render(request, 'main/exp.html')

def gallery(request):
    return render(request, 'main/gallery.html')

def contact(request):
    return render(request, 'main/contact.html')
